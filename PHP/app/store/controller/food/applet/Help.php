<?php
namespace app\store\controller\food\applet;

use app\store\controller\food\Controller;
use app\store\model\Help as HelpModel;
use think\facade\View;

/**
 * 小程序帮助中心
 */
class Help extends Controller
{
    /**
     * 帮助中心列表
     */
    public function index()
    {
        $model = new HelpModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new HelpModel;
        if (!$this->request->isAjax()) {
            return View::fetch('add');
        }
        // 新增记录
        if ($model->add($this->postData('data'))) {
            return $this->renderSuccess('添加成功', url('food.applet.help/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 更新
     */
    public function edit($id)
    {
        // 帮助详情
        $model = HelpModel::get($id);
        if (!$this->request->isAjax()) {
            return $this->fetch('edit', compact('model'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('food.applet.help/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        // 帮助详情
        $model = HelpModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

}
