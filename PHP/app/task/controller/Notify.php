<?php
namespace app\task\controller;

use hema\wechat\Pay as WxPay;
use app\task\model\Setting;
use app\task\model\Order as OrderModel;
use app\task\model\Recharge as RechargeModel;


/**
 * 支付成功异步通知接口
 */
class Notify
{
    /**
     * 支付成功异步通知
     */
    public function order()
    {
		$WxPay = new WxPay([]);
        $WxPay->notify(new OrderModel);
    }

	/**
     * 商家用户退款成功异步通知
     */
    public function orderRefund()
    {
        $WxPay = new WxPay([]);
        $WxPay->notifyRefund(new OrderModel);
    }

    /**
     * 支付成功异步通知 - 充值
     */
    public function recharge()
    {
        $WxPay = new WxPay([]);
        $WxPay->notify(new RechargeModel,'add');
    }

    /**
     * 站点扫码支付成功异步通知
     */
    public function native()
    {
        $values = Setting::getItem('webpay',0);
        $WxPay = new WxPay([]);
        $WxPay->notify(new RechargeModel,'add',$values['wx']['api_key']);
    }
    
    /**
     * 助手小程序充值成功异步通知
     */
    public function foodhelp()
    {
        $values = Setting::getItem('webpay',0);
        $WxPay = new WxPay([]);
        $WxPay->notify(new RechargeModel,'add',$values['wx']['api_key']);
    }
}
