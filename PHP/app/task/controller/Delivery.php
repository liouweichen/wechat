<?php
namespace app\task\controller;

use app\task\model\Order as OrderModel;

/**
 * 第三方配送异步通讯接口
 */

class Delivery
{
	/**
     * 顺丰异步通知处理
     */
    public function shunfeng()
    {
        write_log('顺丰异步通知处理',__DIR__);
		$data = file_get_contents ('php://input' );
		write_log($data,__DIR__);
		$model = OrderModel::getOrder($data['origin_id']);
		$model->updateDeliveryStatus($data,20);
    }

    /**
     * 达达异步通知处理
     */
    public function dada()
    {
		write_log('达达异步通知处理',__DIR__);
		$data = file_get_contents ('php://input' );
		write_log($data,__DIR__);
		$model = OrderModel::getOrder($data['origin_id']);
		$model->updateDeliveryStatus($data,30);
    }

    /**
     * UU异步通知处理
     */
    public function uu()
    {
		write_log('UU异步通知',__DIR__);
		$data = file_get_contents ('php://input' );
		write_log($data,__DIR__);
		$model = OrderModel::getOrder($data['origin_id']);
		$model->updateDeliveryStatus($data,40);
    }
}
