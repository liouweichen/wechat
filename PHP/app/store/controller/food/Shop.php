<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\Shop as ShopModel;
use app\store\model\Setting;
use app\store\model\ShopCategory as ShopCategoryModel;
use think\facade\View;
use hema\wechat\Driver;

/**
 * 门店控制器
 */
class Shop extends Controller
{
	/**
     * 门店地图
     */
    public function getpoint()
    {
		$values = Setting::getItem('web',0);
		View::layout(false);
		View::assign('wxmap_key', $values['wxmap']);
        return View::fetch('getpoint');
    }
	
    /**
     * 列表
     */
    public function index()
    {
        $model = new ShopModel;
        $list = $model->getList(true,15,'',$this->shop_id);
        return View::fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            $model = new ShopCategoryModel;
            $category = $model->getList();
            return View::fetch('add', compact('category'));
        }
        if(!$this->is_admin AND $this->shop_mode == 10){
            return $this->error('单门店版，无权新增门店');
        }
        $model = new ShopModel;
        if ($model->add($this->postData('data'))) {
            return $this->renderSuccess('添加成功', url('food.shop/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = new ShopModel;
        if ($model->remove($id)) {
			return $this->renderSuccess('删除成功');
        }
		$error = $model->getError() ?: '删除失败';
		return $this->renderError($error);
        
    }

    /**
     * 编辑
     */
    public function edit($id = 0)
    {
        $url = 'food.shop/index';
        if($id == 0 AND !$this->is_admin){
            $id = $this->shop_id;
            $url = 'food.shop/edit';
        }
        // 门店详情
        $model = ShopModel::get($id);
        if (!$this->request->isAjax()) {
            $shop_category = new ShopCategoryModel;
            $category = $shop_category->getList();
            return View::fetch('edit', compact('model','category'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url($url));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 状态编辑
     */
    public function status($id)
    {
        $model = ShopModel::get($id);
		$model->status['value'] ? $model->status = 0 : $model->status = 1;
		$model->save();
        return $this->renderSuccess('更新成功', url('food.shop/index'));
    }

    /**
     * 二维码
     */
    public function qrcode($id)
    {
        $wx = new Driver;
        if($url = $wx->getQrCode($this->applet_id,'shop-'.$id)){
            return redirect($url);
        }
        $this->error('二维码获取失败');
    }	
}
