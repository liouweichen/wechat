<?php
namespace app\common\model;

/**
 * 用户收货地址模型
 */
class Address extends BaseModel
{
    // 定义表名
    protected $name = 'address';

    // 定义主键
    protected $pk = 'address_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->hasOne('app\\common\\model\\User');
    }
    
    /**
     * 获取列表
     */
    public function getList($user_id = 0, string $search = '')
    {
        // 筛选条件
        $filter = [];
        $user_id > 0 && $filter['user_id'] = $user_id;
        if(!empty($search)){  
            //是否是手机号
            if(is_phone($search)){
                $filter['phone'] = $search;
            }else{
                $filter['name'] = ['like', '%' . trim($search) . '%'];
            }
        }
        // 执行查询
        $list = $this->with(['user'])
            ->order('address_id','desc')
            ->where($filter)
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
        return $list;
    }

    /**
     * 新增收货地址
    */ 
    public function add(array $data, $user)
    {
        // 添加收货地址
        $data['user_id'] = $user['user_id'];
        $data['applet_id'] = self::$applet_id;
        $this->save($data);
        // 设为默认收货地址
        !$user['address_id'] && $user->save(['address_id' => $this->address_id]);
        return true;
    }
    
    
    /**
     * 编辑地址
    */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 设为默认收货地址
     */
    public function setDefault($user)
    {
        // 设为默认地址
        return $user->save(['address_id' => $this->address_id]);
    }

    /**
     * 删除收货地址
     */
    public function remove($user)
    {
        // 查询当前是否为默认地址
        $user['address_id'] == $this->address_id && $user->save(['address_id' => 0]);
        return $this->delete();
    }
}
