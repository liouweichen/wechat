<?php
namespace app\common\model;

/**
 * 商品图片模型
 */
class GoodsImage extends BaseModel
{
    // 定义表名
    protected $name = 'goods_image';

    // 定义主键
    protected $pk = 'id';
    protected $updateTime = false;
    
    /**
     * 关联文件库
     */
    public function file()
    {
        return $this->belongsTo('app\\common\\model\\UploadFile', 'image_id', 'file_id')
            ->bind(['file_path', 'url', 'domain']);
    }
}
