<?php
namespace app\index\model;

use app\common\model\User as UserModel;
use think\facade\Session;

/**
 * 用户模型
 */
class User extends UserModel
{
    /**
     * 商家用户登录
     */
    public function login($data)
    {
		$filter = [
            'user_name' => $data['user_name'],
            'password' => hema_hash($data['password'])
        ];
        // 验证用户名密码是否正确
		if($user = $this->withoutGlobalScope()->where('status','>',10)->where($filter)->find()){
			// 保存登录状态
			Session::set('hema_store', [
				'user' => $user,
				'is_login' => true,
				'is_admin' => true,
			]);
			return true;
		}else{
			$this->error = '登录失败, 用户名或密码错误';
            return false;
		}
    }  

}
