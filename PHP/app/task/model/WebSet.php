<?php
namespace app\task\model;
use think\Model;

/**
 * 站点配置模型
 */
class WebSet extends Model
{
   	protected $name = 'web_set';
    protected $createTime = false;

    /**
     * 获取器: 转义数组格式
     */
    public function getValuesAttr($value)
    {
        return json_decode($value, true);
    }

    /**
     * 修改器: 转义成json格式
     */
    public function setValuesAttr($value)
    {
        return json_encode($value);
    }
}
