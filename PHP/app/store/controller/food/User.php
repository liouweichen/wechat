<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\Shop as ShopModel;
use app\store\model\User as UserModel;
use think\facade\View;

/**
 * 用户管理
 */
class User extends Controller
{
	
	/**
     * 用户列表
     */
    public function index(string $gender = '', string $search = '')
    {
        $model = new ShopModel;
        $shop = $model->getList(false);
        $model = new UserModel;
        $list = $model->getList(10,$gender, $search);
        return View::fetch('index', compact('list','shop','gender','search'));
    }
	
	/**
     * 用户充值
	 *$recharge 接收表单数据（数组）
	 *$source 0为充值余额，1为充值积分
    */
    public function recharge($user_id, array $recharge, $source)
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $recharge['points']['shop_id'] = $this->shop_id;
            $recharge['balance']['shop_id'] = $this->shop_id;
        }
        $model = new RechargeModel;
        if($model->add($user_id,$recharge,$source)){
             return $this->renderSuccess('操作成功', url('food.user/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
