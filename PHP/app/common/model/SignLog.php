<?php
namespace app\common\model;

use think\facade\Db;

/**
 * 签到记录模型
 */
class SignLog extends BaseModel
{
    // 定义表名
    protected $name = 'sign_log';

    // 定义主键
    protected $pk = 'sign_log_id';

    // 追加字段
    protected $append = [];

    /**
     * 获取今天签到详情
     */
    public function getIsSign($user_id)
    {
        $beginToday = mktime(0,0,0,date('m'),date('d'),date('Y'));
        return $this->where('user_id',$user_id)->where('create_time','>',$beginToday)->count();
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        $data['applet_id'] = self::$applet_id;
        // 开启事务
        Db::startTrans();
        try {
            // 添加记录
            $this->save($data);
            // 增加积分
            $user = User::get($data['user_id']);
            $user->score = ['inc', 1];
            $user->save();
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;  
    }
}
