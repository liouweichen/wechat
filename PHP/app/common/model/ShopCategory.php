<?php
namespace app\common\model;

/**
 * 门店分类模型
 */
class ShopCategory extends BaseModel
{
    // 定义表名
    protected $name = 'shop_category';

    // 定义主键
    protected $pk = 'shop_category_id';

    // 追加字段
    protected $append = [];

    /**
     * 分类图片
     */
    public function image()
    {
        return $this->hasOne('uploadFile', 'file_id', 'image_id');
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        // 排序规则
        $sort = [];
        $sort = ['sort' => 'asc']; 
         // 执行查询
        return $this->withoutGlobalScope()->with(['image'])->order($sort)->select();
    }

}
