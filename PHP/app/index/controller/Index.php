<?php
namespace app\index\controller;

use think\facade\View;

/**
 * 页面
 */
class Index extends Controller
{
    public function index()
    {
		View::assign('key', 'index');
		View::assign('title', '首页');
		View::assign('description', '');
		View::assign('keywords', '');
		return View::fetch();
    }
}
