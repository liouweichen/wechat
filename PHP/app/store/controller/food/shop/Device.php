<?php
namespace app\store\controller\food\shop;

use app\store\controller\food\Controller;
use app\store\model\Device as DeviceModel;
use app\store\model\Shop as ShopModel;
use think\facade\View;

/**
 * 云设备控制器
 */
class Device extends Controller
{
     /**
     * 列表
     */
    public function index($shop_id = 0)
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
        $shop = new ShopModel;
        $category = $shop->getList(false);
        $model = new DeviceModel;
        $list = $model->getList($shop_id);
        return View::fetch('index', compact('list','category','shop_id'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            $model = new ShopModel;
            $category = $model->getList(false);
            return View::fetch('add', compact('category'));
        }
        $data = $this->postData('data');
        if(!$this->is_admin OR $this->shop_mode == 10){
            $data['shop_id'] = $this->shop_id;
        }
        $model = new DeviceModel;
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('food.shop.device/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = DeviceModel::get($id);
        if($model->remove()) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        // 详情
        $model = DeviceModel::get($id);
        if (!$this->request->isAjax()) {
            $shop = new ShopModel;
            $category = $shop->getList(false);
            return View::fetch('edit', compact('model', 'category'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('food.shop.device/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 开启/关闭
     */
    public function status($id)
    {
        $model = DeviceModel::get($id);
        // 更新记录
        if ($model->statu()) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
