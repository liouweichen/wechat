<?php
namespace app\store\model;

use app\common\model\Goods as GoodsModel;
use think\facade\Db;

/**
 * 商品模型
 */
class Goods extends GoodsModel
{
    /**
     * 添加
     */
    public function add(array $data, $shop_id)
    {
        if (!isset($data['images']) || empty($data['images'])) {
            $this->error = '请上传商品图片';
            return false;
        }
		if (empty($shop_id)) {
            $this->error = '请选择门店';
            return false;
        }
		if (empty($data['category_id'])) {
            $this->error = '请选择商品分类';
            return false;
        }
		$data['shop_id'] = $shop_id;
        $data['content'] = isset($data['content']) ? $data['content'] : '';
        $data['applet_id'] = $data['spec']['applet_id'] = self::$applet_id;

        // 开启事务
        Db::startTrans();
        try {
            // 添加商品
            $this->save($data);
            // 商品规格
            $this->addGoodsSpec($data);
            // 商品图片
            $this->addGoodsImages($data['images']);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;

			
    }

    /**
     * 添加商品图片
     */
    private function addGoodsImages(array $images)
    {
        $this->image()->delete();
        $data = array_map(function ($image_id) {
            return [
                'image_id' => $image_id,
                'applet_id' => self::$applet_id
            ];
        }, $images);
        return $this->image()->saveAll($data);
    }

    /**
     * 编辑
     */
    public function edit(array $data)
    {
        if (!isset($data['images']) || empty($data['images'])) {
            $this->error = '请上传商品图片';
            return false;
        }
		if (empty($data['category_id'])) {
            $this->error = '请选择商品分类';
            return false;
        }
        $data['content'] = isset($data['content']) ? $data['content'] : '';
        $data['applet_id'] = $data['spec']['applet_id'] = self::$applet_id;
        // 开启事务
        Db::startTrans();
        try {
            // 保存商品
            $this->save($data);
            // 商品规格
            $this->addGoodsSpec($data, true);
            // 商品图片
            $this->addGoodsImages($data['images']);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
            $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * 添加商品规格
     */
    private function addGoodsSpec(&$data, $isUpdate = false)
    {
        // 更新模式: 先删除所有规格
        $model = new GoodsSpec;
        $isUpdate && $model->removeAll($this['goods_id']);
        // 添加规格数据
        if ($data['spec_type'] == '10') {
            // 单规格
            $this->spec()->save($data['spec']);
        } else if ($data['spec_type'] == '20') {
            // 添加商品与规格关系记录
            $model->addGoodsSpecRel($this['goods_id'], $data['spec_many']['spec_attr']);
            // 添加商品sku
            $model->addSkuList($this['goods_id'], $data['spec_many']['spec_list']);
        }
    }

    /**
     * 删除
     
    public function remove()
    {
        // 开启事务处理
        Db::startTrans();
        try {
            // 删除商品sku
            (new GoodsSpec)->removeAll($this['goods_id']);
            // 删除商品图片
            $this->image()->delete();
            // 删除当前商品
            $this->delete();
            // 事务提交
            Db::commit();
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            Db::rollback();
            return false;
        }  
        
    }
    */
}
