<?php
namespace hema\device\engine;

use hema\Http;

/**
 * 河马云叫号器
 */
class Hmcalling extends Basics
{
    /**
     * 构造方法
     */
    public function __construct($dev_type, $device, $data, $tpl)
    {
        parent::__construct($dev_type, $device, $data, $tpl);
    }

    /**
    * 推送消息
    */
    public function push()
    {
        $params = $this->params();
        $params['device_name'] = $this->data['dev_id'];//设备ID
        $params['mode'] = $this->data['mode'];//播报模式
        $params['row_no'] = $this->data['row_no'];
        $url = $this->config['api_url'] . 'device/push';
        $result = json_decode(Http::post($url,$params),true);
        return $result;//返回数据格式：{"code":0,"msg":"成功"}
    }
    
    /**
    * 授权绑定设备
    */
    public function add()
    {
        $data = $this->data;
        $params = $this->params();
        $params['calling_name'] = $data['dev_name'];//自定义名称(可填)
        $params['device_name'] = $data['dev_id'];//设备ID
        $params['device_key'] = $data['dev_key'];//设备密钥
        $url = $this->config['api_url'] . 'device/add';
        $result = json_decode(Http::post($url,$params),true);
        return $result;//返回数据格式：{"code":0,"msg":"成功"}
    }

    /**
    * 修改绑定设备
    */
    public function edit()
    {
        $data = $this->data;
        $params = $this->params();
        $params['calling_name'] = $data['dev_name'];//自定义名称(可填)
        $params['device_name'] = $data['dev_id'];//设备ID
        $params['device_key'] = $data['dev_key'];//设备密钥
        $url = $this->config['api_url'] . 'device/edit';
        $result = json_decode(Http::post($url,$params),true);
        return $result;//返回数据格式：{"code":0,"msg":"成功"}
    }
    
    /**
    * 删除授权绑定的设备
    */
    public function delete(){
        $params = $this->params();
        $params['device_name'] = $this->data['dev_id'];//设备ID
        $url = $this->config['api_url'] . 'device/delete';
        $result = json_decode(Http::post($url,$params),true);
        return $result;//返回数据格式：{"code":0,"msg":"成功"}
    }
    
    /**
    * 获取设备状态接口 
    */
    public function status(){
        $params = $this->params();
        $params['device_name'] = $this->data['dev_id'];//编号
        $url = $this->config['api_url'] . 'device/status';
        $result = json_decode(Http::post($url,$params),true);
        return $result['text'];
        //返回格式 {'value':状态值,'text':状态}
    }

    public function getToken()
    {

    }
    public function print()
    {
        
    }
    
    /**
    * 公共参数
    */
    private function params(){
        $time = time();         //请求时间
        return $params = [
            'secret_id' => $this->config['app_key'],
            'secret_key' => $this->config['app_secret'],
            'timestamp' => $time,
            'sign' => $this->getSign($time)
        ];
    }
    
    /**
    * 生成签名
    */
    private function getSign($timestamp)
    {
        return md5($this->config['app_key'] . $timestamp . $this->config['app_secret']);
    }
}
