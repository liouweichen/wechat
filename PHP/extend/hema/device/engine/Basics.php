<?php
namespace hema\device\engine;

use app\common\model\Setting;

/**
 * 设备引擎抽象类
 */
abstract class Basics
{
    protected $config;// 云平台参数
    protected $device;// 设备配置
    protected $data;
    protected $tpl;

    // 错误信息
    protected $error;

    /**
     * 构造函数
     */
    public function __construct($dev_type, $device, $data, $tpl)
    {
        $this->device = $device;
        $this->data = $data;
        $this->tpl = $tpl;
        if($dev_type == 'hmcalling'){
            $this->config = Setting::getItem('calling',0);
        }else{
            $this->config = Setting::getItem('printer',0)[$dev_type];
        }
    }

    /**
     * 执行打印
     */
    abstract protected function print();

    /**
     * 播报语音
     */
    abstract protected function push();

    /**
     * 获取设备状态
     */
    abstract protected function status();

    /**
     * 添加设备
     */
    abstract protected function add();

    /**
     * 编辑设备
     */
    abstract protected function edit();

    /**
     * 删除设备
     */
    abstract protected function delete();

    /**
     * 获取token
     */
    abstract protected function getToken();

    /**
     * 返回错误信息
     */
    public function getError(): string
    {
        return $this->error;
    }

}
