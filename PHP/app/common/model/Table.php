<?php
namespace app\common\model;

/**
 * 餐桌/包间模型
 */
class Table extends BaseModel
{
    // 定义表名
    protected $name = 'table';

    // 定义主键
    protected $pk = 'table_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\Shop');
    }
    
    /**
     * 状态
     */
    public function getStatusAttr($value,$data)
    {
        $status = [10 => '空闲', 20 => '忙碌', 30 => '预定'];
        $order_id = '';
        $pay_price = '';
        $time = '';
        if($order = Order::where(['table_id'=>$data['table_id'],'order_status'=>10])->find()){
            $value = 20;
            $order_id = $order['order_id'];
            $pay_price = $order['pay_price'];
            $time = round((time() - strtotime($order['create_time']))/60);
        }
        return [
            'text' => $status[$value], 
            'value' => $value, 
            'order_id' => $order_id, 
            'time' => $time,
            'pay_price' => $pay_price
        ];
    }

    /**
     * 获取列表
     */
    public function getList($shop_id = 0, $status = 0, string $search = '')
    {
        //筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        !empty($search) && $filter['table_name'] = $search;
        // 执行查询
        $list = $this->with(['shop'])
            ->where($filter)
            ->order(['sort','table_id' => 'desc'])
            ->select();
        if($status > 0){
            $tablelist = $list;
            $list = [];
            for($n=0;$n<sizeof($tablelist);$n++){
                if($tablelist[$n]['status']['value'] == $status){
                    array_push($list,$tablelist[$n]);
                }
            }
        }
        return $list;
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);
    }
    
    
    /**
     * 编辑
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }

}
