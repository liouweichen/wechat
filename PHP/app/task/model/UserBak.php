<?php
namespace app\task\model;

use think\Model;

/**
 * 商户模型
 */
class UserBak extends Model
{
    // 定义表名
    protected $name = 'user_bak';

    // 定义主键
    protected $pk = 'user_bak_id';

    // 追加字段
    protected $append = [];
}
