<?php
namespace app\admin\controller;

use app\admin\model\Applet as AppletModel;
use app\admin\model\User as UserModel;
use think\facade\View;

/**
 * 商户后台首页
 */
class Index extends Controller
{
    
    public function index()
    {
    	$count = array();
		$count['applet'] = AppletModel::getCount();		//小程序
		$count['user'] = UserModel::getCount();	//用户统计
		$count['printer'] = 0;	//打印机统计
        return View::fetch('index', compact('count'));
    }
}
