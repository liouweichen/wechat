<?php
return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'apply' => [
        'name' => '用户申请',
        'icon' => 'iconrenzheng',
        'index' => 'apply/applet',
        'submenu' => [
            [
                'name' => '小程序申请',
                'index' => 'apply/applet',
                'uris' => [
                    'apply/applet',
                ],
            ],
            [
                'name' => '平台入驻',
                'index' => 'apply/out',
                'uris' => [
                    'apply/out',
                ],
            ],
            [
                'name' => '支付申请',
                'index' => 'apply/pay',
                'uris' => [
                    'apply/pay',
                ],
            ],
            [
                'name' => '代理认证',
                'index' => 'apply/agent',
                'uris' => [
                    'apply/agent',
                ],
            ],
            [
                'name' => '实名认证',
                'index' => 'apply/auth',
                'uris' => [
                    'apply/auth',
                ],
            ],
            [
                'name' => '提现申请',
                'index' => '#',
                'uris' => [
                    'apply.order/index',
                    'apply.order/cash',
                ],
            ],
        ]
    ],
    'applet' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'applet/all',
        'submenu' => [
            [
                'name' => '小程序管理',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '全部',
                        'index' => 'applet/all',
                        'uris' => [
                            'applet/all',
                            'applet/delete'
                        ],
                    ],
                    [
                        'name' => '已授权',
                        'index' => 'applet/normal',
                        'uris' => [
                            'applet/normal',
                            'applet/delete'
                        ],
                    ],
                    [
                        'name' => '已到期',
                        'index' => 'applet/ends',
                        'uris' => [
                            'applet/ends',
                            'applet/delete'
                        ],
                    ],
                ]
            ],
            [
                'name' => '上线模板',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '草稿库',
                        'index' => 'applet.wxtpl/index',
                        'uris' => [
                            'applet.wxtpl/index',
                            'applet.wxtpl/add'
                        ],
                    ],
                    [
                        'name' => '模板库',
                        'index' => 'applet.wxtpl/lists',
                        'uris' => [
                            'applet.wxtpl/lists',
                            'applet.wxtpl/delete'
                        ],
                    ],
                    [
                        'name' => '推送模板',
                        'index' => 'applet.code/index',
                        'uris' => [
                            'applet.code/index',
                            'applet.code/add',
                            'applet.code/edit',
                            'applet.code/delete'
                        ],
                    ],
                ]
            ],
            [
                'name' => '模板市场',
                'index' => 'applet.template/index',
                'uris' => [
                    'applet.template/index',
                    'applet.template/add',
                    'applet.template/edit',
                    'applet.template/delete'
                ],
            ],
        ],
    ],
    'wechat' => [
        'name' => '公众号',
        'icon' => 'iconweixingongzhonghao',
        'color' => '#36b313',
        'index' => 'wechat/index',
        'submenu' => [
            [
                'name' => '基础信息',
                'index' => 'wechat/index',
                'urls' => [
                    'wechat/index',
                ] 
            ],
            [
                'name' => '菜单设置',
                'index' => 'wechat/menus',
                'urls' => [
                    'wechat/menu',
                ] 
            ],
            [
                'name' => '模板消息',
                'index' => 'wechat.tplmsg/setting',
                'uris' => [
                    'wechat.tplmsg/setting'
                ],
            ],
            [
                'name' => '群发消息',
                'index' => 'wechat.send/index',
                'uris' => [
                    'wechat.send/index',
                    'wechat.send/add',
                    'wechat.send/edit',
                    'wechat.send/delete'
                ],
            ],
            [
                'name' => '素材管理',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '图文素材',
                        'index' => 'wechat.material.text/index',
                        'urls' => [
                            'wechat.material.text/index',
                            'wechat.material.text/add',
                            'wechat.material.text/edit',
                            'wechat.material.text/delete'
                        ]
                    ],
                    [
                        'name' => '图片素材',
                        'index' => 'wechat.material.image/index',
                        'urls' => [
                            'wechat.material.image/index',
                            'wechat.material.image/add',
                            'wechat.material.image/edit',
                            'wechat.material.image/delete'
                        ]
                    ],  
                    [
                        'name' => '语音素材',
                        'index' => 'wechat.material.voice/index',
                        'urls' => [
                            'wechat.material.voice/index',
                            'wechat.material.voice/add',
                            'wechat.material.voice/edit',
                            'wechat.material.voice/delete'
                        ]
                    ],
                    [
                        'name' => '视频素材',
                        'index' => 'wechat.material.video/index',
                        'urls' => [
                            'wechat.material.video/index',
                            'wechat.material.video/add',
                            'wechat.material.video/edit',
                            'wechat.material.video/delete'
                        ]
                    ],
                ]
            ],
            [
                'name' => '智能回复',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '被关注回复',
                        'index' => 'wechat/subscribe',
                            'urls' => [
                                'wechat/subscribe',
                            ]
                    ],
                    [
                        'name' => '关键字回复',
                        'index' => 'wechat.keyword/index',
                        'uris' => [
                            'wechat.keyword/index',
                            'wechat.keyword/add',
                            'wechat.keyword/edit',
                            'wechat.keyword/delete',
                        ],
                    ],
                    
                ]
            ],
        ],
    ],
    'pay' => [
        'name' => '支付设置',
        'icon' => 'iconcaiwuguanli',
        'color' => '#36b313',
        'index' => 'pay/webpay',
        'submenu' => [
            [
                'name' => '支付设置',
                'index' => 'pay/webpay',
                'uris' => [
                    'pay/webpay',
                ],
            ],
            [
                'name' => '服务商设置',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '微信支付',
                        'index' => 'pay/wxpayisp'
                    ],
                    
                ]
            ],
        ]
    ],
    'out' => [
        'name' => '外卖平台',
        'icon' => 'iconwaimai',
        'color' => '#f37b1d',
        'index' => '#',
        'submenu' => [
            [
                'name' => '门店管理',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '门店列表',
                        'index' => 'out.shop/index',
                            'urls' => [
                                'out.shop/index',
                            ]
                    ],
                    [
                        'name' => '门店分类',
                        'index' => 'out.shop.category/index',
                        'uris' => [
                            'out.shop.category/index',
                            'out.shop.category/add',
                            'out.shop.category/edit',
                            'out.shop.category/delete',
                        ],
                    ],
                    
                ]
            ],
            [
                'name' => '页面管理',
                'index' => 'out.page/index',
                'uris' => [
                    'out.page/index',
                    'out.page/add',
                    'out.page/edit',
                    'out.page/delete',
                ],
            ],
            [
                'name' => '平台设置',
                'active' => false,
                'submenu' => [
                    [
                        'name' => 'APP对接',
                        'index' => 'out.setting/outapp',
                        'uris' => [
                            'out.setting/outapp',
                        ],
                    ],
                    [
                        'name' => '结算设置',
                        'index' => 'out.setting/solve',
                        'uris' => [
                            'out.setting/solve',
                        ],
                    ],
                    
                ]
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'user/store',
        'submenu' => [
            [
                'name' => '商家用户',
                'index' => 'user/store',
                'uris' => [
                    'user/store',
                ],
            ],
            [
                'name' => '代理用户',
                'index' => 'user/agent',
                'uris' => [
                    'user/agent',
                ],
            ],
            [
                'name' => '商家会员',
                'index' => 'user/user',
                'uris' => [
                    'user/user',
                ],
            ],
            [
                'name' => '支付记录',
                'index' => 'user/pay',
                'uris' => [
                    'user/pay',
                ],
            ],
        ]
    ],
    'open' => [
        'name' => '开放平台',
        'icon' => 'iconkaifangpingtai',
        'index' => 'open/wxopen',
        'submenu' => [
            [
                'name' => '微信平台',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '第三方应用',
                        'index' => 'open/wxopen',
                        'urls' => [
                            'open/wxopen'
                        ]
                    ],
                    [
                        'name' => '网站应用',
                        'index' => 'open/wxweb',
                        'urls' => [
                            'open/wxweb'
                        ]
                    ],
                ]
            ],
            [
                'name' => '第三方配送',
                'index' => 'open/webdelivery',
                'urls' => [
                    'open/webdelivery'
                ]
            ],
            [
                'name' => '云打印机',
                'index' => 'open/printer',
                'urls' => [
                    'open/printer',
                ]
            ],
            [
                'name' => '云叫号器',
                'index' => 'open/calling',
                'urls' => [
                    'open/calling',
                ]
            ],
           
        ]
    ],  
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'setting/web',
        'submenu' => [
            [
                'name' => '站点设置',
                'index' => 'setting/web',
                'urls' => [
                    'setting/web'
                ]
            ],
            [
                'name' => '助手设置',
                'index' => 'setting/foodhelp',
                'urls' => [
                    'setting/foodhelp'
                ]
            ],
            /*[
                'name' => '注册设置',
                'index' => 'setting/register',
                'urls' => [
                    'setting/register'
                ]
            ],*/
            [
                'name' => '友情链接',
                'index' => 'setting.link/index',
                'urls' => [
                    'setting.link/index',
                    'setting.link/add',
                    'setting.link/edit',
                    'setting.link/delete'
                ]
            ],
            [
                'name' => '其他',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '清理缓存',
                        'index' => 'setting.cache/clear'
                    ],
                    [
                        'name' => '环境监测',
                        'index' => 'setting.science/index'
                    ]
                ]
            ],
        ],
    ],
];
