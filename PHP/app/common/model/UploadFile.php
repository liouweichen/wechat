<?php

namespace app\common\model;

//use think\model\concern\SoftDelete;

/**
 * 文件库模型
 */
class UploadFile extends BaseModel
{
    //use SoftDelete;

    // 定义表名
    protected $name = 'upload_file';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 定义主键
    protected $pk = 'file_id';

    // 追加的字段
    protected $append = ['url'];

    /**
     * 获取图片完整路径
     */
    public function getUrlAttr($value, $data)
    {
        // 存储方式本地：拼接当前域名
        if ($data['storage'] === 'local') {
            $data['domain'] = uploads_url();
        }
        return "{$data['domain']}/{$data['file_path']}";
    }

    /**
     * 根据文件名查询文件id
     */
    public static function getFildIdByName(string $file_path)
    {
        return (new static)->withoutGlobalScope()->where(['file_path' => $file_path])->value('file_id');
    }

    /**
     * 查询文件id
     */
    public static function getFileName($fileId)
    {
        return (new static)->withoutGlobalScope()->where(['file_id' => $fileId])->value('file_path');
    }

    /**
     * 获取列表记录
     */
    public function getList($group_id = 0, string $file_type = 'image')
    {
        $applet_id = self::$applet_id;
        $filter = [];
        $filter['file_type'] = $file_type;
        $filter['is_delete'] = 0;
        empty($applet_id) && $filter['applet_id'] = 0;
        $model = $this->where($filter);
        if ($group_id !== 0) {
            $model->where(compact('group_id'));
        }
        return $model->order(['file_id' => 'desc'])->paginate(32);
    }


    /**
     * 文件详情
     */
    public static function detail($fileId)
    {
        return self::withoutGlobalScope()->where('file_id',$fileId)->find();
    }

    /**
     * 添加新记录
     */
    public function add(array $data)
    {
        $applet_id = self::$applet_id;
        empty($applet_id) && $applet_id = 0;
        $data['applet_id'] = $applet_id;
        return $this->save($data);
    }

    /**
     * 批量软删除
     */
    public function softDelete(array $fileIds)
    {
        return $this->withoutGlobalScope()->where('file_id','in',$fileIds)->update(['is_delete' => 1]);
    }

    /**
     * 批量移动文件分组
     */
    public function moveGroup($group_id, array $fileIds)
    {   
        return $this->withoutGlobalScope()->where('file_id','in',$fileIds)->where($filter)->update(compact('group_id'));
    }

}
