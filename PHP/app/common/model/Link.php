<?php
namespace app\common\model;

use think\facade\Cache;

/**
 * 友情链接模型
 */
class Link extends BaseModel
{
    // 定义表名
    protected $name = 'link';

    // 定义主键
    protected $pk = 'link_id';

    // 追加字段
    protected $append = [];

    /**
     * 所有分类
     */
    public static function getList()
    {
        $model = new static;
        if (!Cache::get('hema_link')) {
            $link = $model->withoutGlobalScope()->order(['sort' => 'asc'])->select();
            Cache::set('hema_link', $link);
        }
        return Cache::get('hema_link');
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        $this->deleteCache();
        return $this->save($data);
    }

    /**
     * 编辑
     */
    public function edit(array $data)
    {
        $this->deleteCache();
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        $this->deleteCache();
        return $this->delete();
    }

    /**
     * 删除缓存
     */
    private function deleteCache()
    {
        return Cache::delete('hema_link');
    }
}
