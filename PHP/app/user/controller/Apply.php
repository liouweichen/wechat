<?php
namespace app\user\controller;

use app\user\model\Apply as ApplyModel;
use think\facade\View;

/**
 * 用户认证申请控制器
 */
class Apply extends Controller
{
    /**
     * 获取申请列表
     */
    public function index()
    {
        $model = new ApplyModel;
        $list = $model->getList(0,$this->user_id);
        return View::fetch('index',compact('list'));
    }

    /**
     * 编辑
     */
    public function detail($id)
    {   

        $model = ApplyModel::detail($id);
        if(!$this->request->isAjax()) {
            return View::fetch('detail', compact('model'));
        }
        //提交动作
        $data = $this->postData('data');
        $data['apply_status'] = 10;//驳回后提交
        if ($model->action($data)) {
            return $this->renderSuccess('修改成功', url('apply/index'));
        }
        $error = $model->getError() ?: '修改失败';
        return $this->renderError($error);      
    }
   
}
