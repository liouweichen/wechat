<?php
namespace app\admin\model;

use app\common\model\Applet as AppletModel;

/**
 * 小程序模型
 */
class Applet extends AppletModel
{
	
	/**
     * 变更到期时间操作
    */
    public function genewal(array $data)
    {
		if($data['term']<1){
			$this->error = '变更数值要大于0';
			return false;
		}
		//计算变更时间
		if($data['term_unit']==10){
			$expire_time = 86400*$data["term"];//计算天
		}
		if($data['term_unit']==20){
			$expire_time = 86400*30*$data["term"];//计算月
		}
		if($data['term_unit']==30){
			$expire_time = 86400*365*$data["term"];//计算年
		}
		//增加
		if($data['mode']=='inc'){
			$expire_time = $this['expire_time']['value']+$expire_time;
		}
		//扣减
		if($data['mode']=='dec'){
			$expire_time = $this['expire_time']['value']-$expire_time;
		}
		//重置
		if($data['mode']=='final'){
			$expire_time = time()+$expire_time;
		}
		return $this->save(['expire_time' => $expire_time]) !== false;
    }

}

