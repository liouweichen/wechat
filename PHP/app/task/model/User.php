<?php
namespace app\task\model;

use app\common\model\User as UserModel;

/**
 * 商户模型
 */
class User extends UserModel
{
    /**
     * 关注公众号
     */
    public function subscribe(array $data, $applet_id)
    {
        $data['is_subscribe'] = 1;
        //商家的公众号
        if($applet_id > 0){
            $user_name = true;
            //商家用户
            if(!$model = User::getUser(['open_id' => $data['open_id'],'status' => 10])){
                $model = $this; 
                $data['status'] = 10;
            }
            return $model->save($data);
        }
        //站点的公众号
        $data['password'] = hema_hash('123456');
        //站点用户(商家管理员)
        if(!$model = User::getUser(['union_id' => $data['union_id'],'status' => 20])){
            $user_name = time();
            $model = $this; 
            $data['status'] = 20;
            $data['user_name'] = $user_name;
        }else{
            if(empty($model['user_name'])){
               $user_name = time(); 
               $data['user_name'] = $user_name;
            }else{
               $user_name = $model['user_name']; 
            }
            
        }
        $model->save($data);
        return $user_name;
    }

    /**
     * 找回账号
     */
    public function retrieve()
    {
        return $this->save(['password' => hema_hash('123456')]) !== false;
    }

    /**
     * 公众号取消关注
     */
    public function unSubscribe()
    {
        return $this->save(['is_subscribe' => 0]) !== false;
    }
}
