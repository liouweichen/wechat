<?php
namespace hema\device;

use hema\device\engine\Mstching;
use hema\device\engine\Feieyun;
use hema\device\engine\Yilianyun;
use hema\device\engine\Hmcalling;

/**
 * 云设备模块驱动
 */
class Driver
{
    private $engine;    // 当前设备擎类

    /**
     * 构造方法
     * Driver constructor.
     * @param $device
     * @param null|string $dev_type 指定设备类型，如不指定则为系统默认
     * @param  $data 数据
     * @param $tpl 0=新订单 1=退款订单
     * @throws Exception
     */
    public function __construct($device = [], $data = [], $tpl = 0, $dev_type = null)
    {
        $this->engine = $this->getEngineClass($dev_type,$device,$data,$tpl);// 实例化当前设备引擎
    }

    /**
     * 获取当前的存储引擎
     * @param null|string $dev_type 指定设备，如不指定则为系统默认
     * @return mixed
     * @throws Exception
     */
    private function getEngineClass($dev_type = null,$device,$data,$tpl)
    {
        $dev_type = is_null($dev_type) ? $device['dev_type']['value'] : $dev_type;
        if($dev_type == 'mstching'){
            return new Mstching($dev_type, $device,$data,$tpl);
        }
        if($dev_type == 'feieyun'){
            return new Feieyun($dev_type, $device,$data,$tpl);
        }
        if($dev_type == 'yilianyun'){
            return new Yilianyun($dev_type, $device,$data,$tpl);
        }
        if($dev_type == 'hmcalling'){
            return new Hmcalling($dev_type, $device,$data,$tpl);
        }
        die(json_encode(['code' => 0, 'msg' => '未找到设备引擎类:' . $dev_type]));
    }

    /**
     * 执行打印
     */
    public function print()
    {
        return $this->engine->print();
    }

    /**
     * 播报语音
     */
    public function push()
    {
        return $this->engine->push();
    }

    /**
     * 获取设备状态
     */
    public function status()
    {
        return $this->engine->status();
    }

    /**
     * 添加设备
     */
    public function add()
    {
        return $this->engine->add();
    }

    /**
     * 编辑设备
     */
    public function edit()
    {
        return $this->engine->edit();
    }

    /**
     * 删除设备
     */
    public function delete()
    {
        return $this->engine->delete();
    }

    /**
     * 获取token
     */
    public function getToken()
    {
        return $this->engine->getToken();
    }

    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError()
    {
        return $this->engine->getError();
    }

}
