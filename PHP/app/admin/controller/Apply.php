<?php
namespace app\admin\controller;

use app\admin\model\Apply as ApplyModel;
use app\admin\model\UserDetail as UserDetailModel;
use think\facade\View;
/**
 * 用户认证申请控制器
 */
class Apply extends Controller
{
    /**
     * 小程序申请
     */
    public function applet()
    {
        return $this->lists(10,'小程序申请');
    }

    /**
     * 平台入驻
     */
    public function out()
    {
		return $this->lists(20,'平台入驻');
    }
    
    /**
     * 支付申请
     */
    public function pay()
    {
		return $this->lists(30,'支付申请');
    }
    /**
     * 代理认证
     */
    public function agent()
    {
		return $this->lists(40,'代理认证');
    }

    /**
     * 实名认证
     */
    public function auth()
    {
		return $this->lists(50,'实名认证');
    }

    /**
     * 用户资料
     */
    private function lists($apply_mode, string $title)
    {
        $model = new ApplyModel;
        $list = $model->getList($apply_mode);
        return View::fetch('index', compact('list','title'));
    }

    /**
     * 详情
    */
    public function detail($id)
    {
        $model = ApplyModel::detail($id);
        if (!$this->request->isAjax()) {
            return View::fetch('detail', compact('model'));
        }
        $data = $this->postData('data');
        if($data['apply_status']==40 AND empty($data['reject'])){
            return $this->renderError('请输入驳回原因');
        }
        $url = [10 => 'applet',20 => 'out',30 => 'pay',40 => 'agent',50 => 'auth'];
        // 更新记录
        if ($model->action($data)) {
            return $this->renderSuccess('更新成功', (string)url('apply/'.$url[$model['apply_mode']['value']]));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
    
}
