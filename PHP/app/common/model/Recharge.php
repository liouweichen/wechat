<?php
namespace app\common\model;

/**
 * 用户充值模型
 */
class Recharge extends BaseModel
{
    // 定义表名
    protected $name = 'recharge';

    // 定义主键
    protected $pk = 'recharge_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联套餐表
     */
    public function plan()
    {
        return $this->belongsTo('app\\common\\model\\RechargePlan');
    }
    
    /**
     * 关联门店
     */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\Shop');
    }
    
    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->belongsTo('app\\common\\model\\User');
    }
    
    /**
     * 扣付状态
     */
    public function getPayStatusAttr($value)
    {
        $status = [10 => '未完成', 20 => '已完成'];
        return ['text' => $status[$value], 'value' => $value];
    }
    
    /**
     * 充值来源
     */
    public function getModeAttr($value)
    {
        $status = [10 => '自助', 20 => '后台'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 扣付类型
     */
    public function getTypeAttr($value)
    {
        $status = [10 => '余额', 20 => '积分'];
        return ['text' => $status[$value], 'value' => $value];
    }

     /**
     * 扣付方式
     */
    public function getActionAttr($value)
    {
        $status = [10 => '新增', 20 => '扣减', 30 => '重置'];
        return ['text' => $status[$value], 'value' => $value];
    }
    
    /**
     * 获取列表
     */
    public function getList($user_type = 0, $user_id = 0, $shop_id = 0)
    {
        // 筛选条件
        $filter = [];
        $user_type > 0 && $filter['user_type'] = $user_type;
        $user_id > 0 && $filter['user_id'] = $user_id;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        // 执行查询
        $list = $this->with(['plan','user','shop'])
            ->where($filter)
            ->order('recharge_id','desc')
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
        return $list;
    }

    /**
     * 订单详情
     */
    public static function detail($id)
    {
        return self::get($id, ['plan','user','shop']);
    }
    
    /**
     * 根据时间段统计数量
     */
    public static function getDateCount(array $shop)
    {
        $self = new static;
         // 筛选条件
        $filter = [];
        $shop['shop_id'] > 0 && $filter['shop_id'] = $shop['shop_id'];
        $applet_id = $self::$applet_id;
        empty($applet_id) && $filter['applet_id'] = 0;
        $filter['pay_status'] = 20;
        $count = self::where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->sum('money');
        return $count;
    }
    
    /**
     * 根据条件统计数量
     */
    public static function getCount($shop_id = 0)
    {
        $self = new static;
         // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $applet_id = $self::$applet_id;
        empty($applet_id) && $filter['applet_id'] = 0;
        $count = array();
        //全部
        $count[0] = self::where('pay_status','=',20)->where($filter)->sum('money');
        //今天
        $star = strtotime(date("Y-m-d"),time());
        $count[1] = self::where('pay_status','=',20)->where('create_time','>',$star)->where($filter)->sum('money');
        //昨天
        $star = strtotime("-1 day");
        $end = strtotime(date("Y-m-d"),time());
        $count[2] = self::where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
        //前天
        $star = strtotime("-2 day");
        $end = strtotime("-1 day");
        $count[3] = self::where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
        //-4天
        $star = strtotime("-3 day");
        $end = strtotime("-2 day");
        $count[4] = self::where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
        //-5天
        $star = strtotime("-4 day");
        $end = strtotime("-3 day");
        $count[5] = self::where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
        //-6天
        $star = strtotime("-5 day");
        $end = strtotime("-4 day");
        $count[6] = self::where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
        //-7天
        $star = strtotime("-6 day");
        $end = strtotime("-5 day");
        $count[7] = self::where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
        
        //本月起至时间 - 月度统计 
        $end = mktime(0,0,0,date('m'),1,date('y')); 
        $count[8] = self::where('pay_status','=',20)->where('create_time','>',$end)->where($filter)->sum('money');
        
        //上月开始  
        $star = mktime(0,0,0,date('m')-1,1,date('y'));
        $count[9] = self::where('pay_status','=',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->sum('money');
        
        return $count;
    }
}
