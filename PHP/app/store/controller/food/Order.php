<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\Order as OrderModel;
use app\store\model\ShopClerk as ShopClerkModel;
use app\store\model\Shop as ShopModel;
use think\facade\View;

/**
 * 订单管理
 */
class Order extends Controller
{
	
	
	/**
     * 全部退款订单列表
     */
    public function refund_list($shop_id = 0, string $search='')
    {
        return $this->getList('全部退款订单列表','refund',$shop_id,$search);
    }
	
	/**
     * 待退款订单列表
     */
    public function refund10_list($shop_id = 0, string $search='')
    {
        return $this->getList('待退款订单列表','refund10',$shop_id,$search);
    }
	
	/**
     * 已退款订单列表
     */
    public function refund20_list($shop_id = 0, string $search='')
    {
        return $this->getList('已退款订单列表','refund20',$shop_id,$search);
    }

    /**
     * 待收款订单列表
     */
    public function collection_list($shop_id = 0, string $search='')
    {
        return $this->getList('待收款订单列表','collection',$shop_id,$search);
    }

    /**
     * 待接单订单列表
     */
    public function shop_list($shop_id = 0, string $search='')
    {
        return $this->getList('待接单订单列表','shop',$shop_id,$search);
    }
	
    /**
     * 待发货订单列表
     */
    public function delivery_list($shop_id = 0, string $search='')
    {
        return $this->getList('待发货订单列表','delivery',$shop_id,$search);
    }

    /**
     * 待收货订单列表
     */
    public function receipt_list($shop_id = 0, string $search='')
    {
        return $this->getList('待收货订单列表','receipt',$shop_id,$search);
    }

    /**
     * 已完成订单列表
     */
    public function complete_list($shop_id = 0, string $search='')
    {
        return $this->getList('已完成订单列表','complete',$shop_id,$search);
    }

    /**
     * 被取消订单列表
     */
    public function cancel_list($shop_id = 0, string $search='')
    {
        return $this->getList('被取消订单列表','cancel',$shop_id,$search);
    }

    /**
     * 全部订单列表
     */
    public function all_list($shop_id = 0, string $search='')
    {
        return $this->getList('全部订单列表','all',$shop_id,$search);
    }

    /**
     * 订单列表
     */
    private function getList(string $title, string $dataType, $shop_id, string $search = '')
    {   
        if(!$this->is_admin OR $this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
        $model = new ShopModel;
        $category = $model->getList(false);
        $model = new OrderModel;
        $list = $model->getList($dataType,$shop_id,0,$search);
        return View::fetch('index', compact('title','list','category','shop_id','search'));
    }

    /**
     * 订单详情
     */
    public function detail($id)
    {
        $detail = OrderModel::detail($id);
		$model = new ShopClerkModel;
		$clerk = $model->getAll($detail['shop_id']);
        return View::fetch('detail', compact('detail','clerk'));
    }

    /**
     * 确认接单
     */
    public function shop($id)
    {
        $model = OrderModel::detail($id);
        if ($model->setShopStatus()) {
            return $this->renderSuccess('操作完成');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 设置外卖配送状态
     */
    public function deliveryStatus($id)
    {
        $model = OrderModel::detail($id);
        if ($model->setDeliveryStatus($this->postData('data'))) {
            return $this->renderSuccess('操作完成');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 确认发货
     */
    public function delivery($id)
    {
        $model = OrderModel::detail($id);
        $delivery = $this->postData('data');
        if($delivery['company']==10 AND isset($delivery['shop_clerk_id'])){
            $delivery['delivery_status'] = 20;
            if ($model->setDeliveryStatus($delivery)) {
                return $this->renderSuccess('操作成功');
            }
            return $this->renderError('操作失败');
        }
        if ($model->setDelivery($delivery['company'])) {
            return $this->renderSuccess('操作完成');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }
	
	/**
     * 退款操作
     */
    public function refund($id)
    {
        $refund = $this->postData('data');
        $model = OrderModel::detail($id);
        if ($model->refund($refund['is_refund'])) {
            return $this->renderSuccess('操作成功');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 确认收到用户付款
     */
    public function collection($id)
    {
        $model = OrderModel::detail($id);
        if ($model->collection()) {
            return $this->renderSuccess('操作成功');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }
}
