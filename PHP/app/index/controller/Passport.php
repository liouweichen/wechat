<?php
namespace app\index\controller;

use app\index\model\User as UserModel;
use app\index\model\Setting;
use think\facade\View;
use think\facade\Session;

/**
 * 用户认证
 */
class Passport extends Controller
{

    /**
     * 用户登录
     */
    public function login()
    {
        if (!$this->request->isAjax()) {
            // 验证登录状态
            if (isset($this->user) AND (int)$this->user['is_login'] === 1) {
                return redirect('/user');
            }
            $values = Setting::getItem('wxweb');
            View::assign('app_id', $values['app_id']);
            View::assign('key', 'user');
            View::assign('title', '用户登录');
            View::assign('description', '');
            View::assign('keywords', '');
            return View::fetch();
        }
        $model = new UserModel;
        if (($model->login($this->postData('data'))) === false) {
            return $this->renderError($model->getError() ?: '登录失败');
        }
        return $this->renderSuccess('登录成功', '/user');
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        // 清空登录状态
        Session::delete('hema_store');
        return redirect('/');
    }
}
