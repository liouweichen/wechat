<?php
namespace app\index\controller;

use think\facade\View;
use think\facade\Session;
use app\index\model\Setting;
use app\index\model\Wechat;
use app\index\model\Link as LinkModel;

/**
 * 管理后台控制器基类
 */
class Controller extends \app\BaseController
{
    protected $user;    // 用户登录信息
    protected $controller = ''; // 当前控制器名称
    protected $action = '';     // 当前方法名称
    protected $routeUri = '';   // 当前路由uri
    protected $group = '';  // 当前路由：分组名称

    /**
     * 后台初始化
     */
    public function initialize()
    {
        $this->getUserInfo();// 获取登录信息
        $this->getRouteInfo();// 当前路由信息
        $this->layout();
    }

    /**
     * 获取登录信息
     */
    private function getUserInfo()
    {
        $this->user = Session::get('hema_store');
    }

    /**
     * 解析当前路由参数 （分组名称、控制器名称、方法名）
     */
    protected function getRouteInfo()
    {
        // 控制器名称
        $this->controller = uncamelize($this->request->controller());
        // 方法名称
        $this->action = $this->request->action();
        // 控制器分组 (用于定义所属模块)
        $group = strstr($this->controller, '.', true);
        $this->group = $group !== false ? $group : $this->controller;
        // 当前uri
        $this->routeUri = "{$this->controller}/$this->action";
    }

    /**
     * 全局layout模板输出
     */
    private function layout()
    {
        $model = new LinkModel;
        $link = $model->getList();
        $web = Setting::getItem('web',0);
        if($wechat = Wechat::getWechat(['applet_id' => 0])){
           $web['qrcode'] = $wechat['qrcode_url'];
       }
        // 输出到view
        View::assign([
            'base_url' => base_url(),            // 当前站点域名
            'web' => $web,    //站点设置
            'store_url' => '/index/',       // 后台模块url
            'group' => $this->group,
            'user' => $this->user,            // 登录信息
            'link' => $link,
        ]);
    }

    /**
     * 返回封装后的 API 数据到客户端
     */
    protected function renderJson($code = 1, string $msg = '', string $url = '', array $data = [])
    {
        return json(compact('code', 'msg', 'url', 'data'));
    }

    /**
     * 返回操作成功json
     */
    protected function renderSuccess(string $msg = 'success', string $url = '', array $data = [])
    {
        return $this->renderJson(1, $msg, $url, $data);
    }

    /**
     * 返回操作失败json
     */
    protected function renderError(string $msg = 'error', string $url = '', array $data = [])
    {
        return $this->renderJson(0, $msg, $url, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postData($key = null)
    {
        return $this->request->post(empty($key) ? '' : "{$key}/a");
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postForm($key = 'form')
    {
        return $this->postData($key);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function getData($key = null)
    {
        return $this->request->get(is_null($key) ? '' : $key);
    }
}
