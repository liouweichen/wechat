<?php
namespace app\common\model;

use think\facade\Session;

/**
 * 用户模型
 */
class User extends BaseModel
{
    // 定义表名
    protected $name = 'user';

    // 定义主键
    protected $pk = 'user_id';

    // 追加字段
    protected $append = [
        'details',
        'usable'
    ];

    /**
     * 计算可用积分
     */
    public function getUsableAttr($value,$data)
    {
        return $data['score'] - $data['converted'];
    }

    /**
     * 关联收货地址表
     */
    public function address()
    {
        return $this->hasMany('app\\common\\model\\Address');
    }

    /**
     * 关联收货地址表 (默认地址)
     */
    public function addressDefault()
    {
        return $this->belongsTo('app\\common\\model\\Address');
    }

    /**
     * 用户资料
     */
    public function getDetailsAttr($value, $data)
    {
        return $this->hasOne('app\\common\\model\\UserDetail');
    }
    /**
     * 显示性别
     */
    public function getGenderAttr($value)
    {
        $status = [0 => '未知', 1 => '先生', 2 => '女士'];
        return $status[$value];
    }
    
    /**
     * 推荐人
     */
    public function getRecommenderAttr($value)
    {
        if($value==0){
            return ['text' => '平台推荐', 'value' => $value];
        }else{
            return ['text' => 'ID:'.$value, 'value' => $value];
        }
    }
    
    /**
     * 用户身份
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '用户', 20 => '管理', 30 => '代理'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 用户头像
     */
    public function getAvatarAttr($value)
    {
        empty($value) && $value = base_url() . 'assets/img/avatar.png';
        return $value;
    }
    
    /**
     * 获取列表
     */
    public function getList($status = 10, string $gender = '', string $search = '', $agent_id = 0)
    {
        $filetr = [];
        $filetr['status'] = $status;
        $agent_id > 0 && $filetr['agent_id'] = $agent_id;
        !empty($gender) && $filetr['gender'] = $gender;
        if(!empty($search)){
            //是否是数字
            if(is_numeric($search)){   
                //是否是手机号
                if(is_phone($search)){
                    $filter['phone'] = $search;
                }else{
                    $filter['user_id'] = $search;
                }
            }else{
               //不是数字   
                $filter['nickname'] = ['like', '%' . trim($search) . '%'];
            }
        }
        // 执行查询
        return $this->where($filetr)
            ->order('user_id','desc')
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 获取详情信息 - 跟据条件
    */
    public static function getUser(array $filter)
    {
        return self::withoutGlobalScope()->where($filter)->find();
    }

    /**
     * 修改
     */
    public function edit(array $data)
    {
        switch ($data['gender']) {
            case '先生':
                $data['gender'] = 1;
                break;
            case '女士':
                $data['gender'] = 2;
                break;
            default:
                $data['gender'] = 0;
                break;
        }
        return $this->save($data) !== false;
    }


    /**
     * 一键登录
     */
    public function oneKey($title = '')
    {
        if($this->status['value'] == 10){
            return false;
        }else{
            $name = 'hema_store';
            $url = '/user';
            if($this->status['value'] == 30 AND $title == '代理用户'){
                $name = 'hema_agent';
                $url = '/agent';
            }
        }
        Session::set($name, [
            'user' => $this,
            'is_login' => true,
            'is_admin' => true,
        ]);
        return $url;
    }

    /**
     * 修改管理员密码
     */
    public function renew(array $data)
    {
        //验证密码长度是否合法
        if(strlen($data['password'])<4){
            $this->error = '新密码长度不足4位';
            return false;
        }
        if ($data['password'] !== $data['password_confirm']) {
            $this->error = '两次输入的新密码不一致';
            return false;
        }
        // 更新管理员信息
        return $this->save([
            'password' => hema_hash($data['password'])
        ]) !== false;
    }
























    

    /**
     * 根据条件统计数量
     */
    public static function getCount($agent_id = 0)
    {
        $self = new static;
        $count = array();
        //全部统计
        $count['all']['all'] = self::count();//全部数量
        $count['all']['user'] = self::where(['status' => 10])->count();//全部用户数量
        $count['all']['agent'] = self::where(['status' => 30])->count();//全部代理数量
        $count['all']['store'] = self::where(['status' => 20])->count();//全部商户
        //余额统计
        $count['money']['all'] = self::sum('money');//全部用户余额
        $count['money']['user'] = self::where(['status' => 10])->sum('money');//站点余额
        $count['money']['store'] = self::where(['status' => 20])->sum('money');//商家余额
        $count['money']['agent'] = self::where(['status' => 30])->sum('money');//代理余额
        //代理数据
        $count['agent']['all'] = self::where(['agent_id' => $agent_id])->count();//代理用户数量
        $count['agent']['money'] = self::where(['user_id' => $agent_id])->sum('money');//代理余额
        $count['agent']['money_store'] = self::where(['agent_id' => $agent_id])->sum('money');//代理商户余额
        //今天统计
        $star = strtotime(date('Y-m-d 00:00:00',time()));
        $count['today']['all'] = self::where(['create_time' => ['>',$star]])->count();//全部数量
        $count['today']['agent'] = self::where(['agent_id' => $agent_id,'create_time' => ['>',$star]])->count();//所属代理数量
        //昨天统计
        $star = strtotime("-1 day");
        $end = strtotime(date('Y-m-d 00:00:00',time()));
        $count['today2']['all'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
        $count['today2']['agent'] = self::where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        //前天统计
        $star = strtotime("-2 day");
        $end = strtotime("-1 day");
        $count['today3']['all'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
        $count['today3']['agent'] = self::where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        //-4天统计
        $star = strtotime("-3 day");
        $end = strtotime("-2 day");
        $count['today4']['all'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
        $count['today4']['agent'] = self::where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        //-5天统计
        $star = strtotime("-4 day");
        $end = strtotime("-3 day");
        $count['today5']['all'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
        $count['today5']['agent'] = self::where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        //-6天统计
        $star = strtotime("-5 day");
        $end = strtotime("-4 day");
        $count['today6']['all'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
        $count['today6']['agent'] = self::where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        //-7天统计
        $star = strtotime("-6 day");
        $end = strtotime("-5 day");
        $count['today7']['all'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
        $count['today7']['agent'] = self::where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        //本月统计 
        $end = mktime(0,0,0,date('m'),1,date('y')); 
        $count['month']['all'] = self::where('create_time','>',$end)->count();//全部数量
        $count['month']['agent'] = self::where('create_time','>',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        //上月统计  
        $star = mktime(0,0,0,date('m')-1,1,date('y'));
        $count['month2']['all'] = self::where('create_time','>',$star)->where('create_time','<',$end)->count();//全部数量
        $count['month2']['agent'] = self::where('create_time','>',$star)->where('create_time','<',$end)->where(['agent_id' => $agent_id])->count();//所属代理数量
        return $count;
    }
    
    /**
     * 平台获取列表
     */
    public function getOutList()
    {
        $where = [];
        // 执行查询
        return $this->withoutGlobalScope()
            ->where($where)
            ->where('out_open_id','<>', '')
            ->order('user_id','desc')
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }
}
