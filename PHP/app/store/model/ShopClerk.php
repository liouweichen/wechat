<?php
namespace app\store\model;

use app\common\model\ShopClerk as ShopClerkModel;
use think\facade\Session;

/**
 * 店员模型
 */
class ShopClerk extends ShopClerkModel
{

    /**
     * 店长登录
     */
    public function login(array $data)
    {
		$filter = [
            'mobile' => $data['user_name'],
            'pwd' => hema_hash($data['password']),
			'status' => 20 //店长
        ];
        // 验证用户名密码是否正确
		if($user = $this->where($filter)->with(['shop'])->find()){
			$applet = Applet::get($user['applet_id']);
            $user['user_id'] = $applet['user_id'];
            $user['user_name'] = $user['mobile'];
            $user['avatar'] = $applet['head_img'];
			// 保存登录状态
			Session::set('hema_store', [
				'user' => $user,
				'applet' => $applet,
				'is_login' => true,
				'is_admin' => false,
			]);
			return true;
		}else{
			$this->error = '登录失败, 用户名或密码错误';
            return false;
		}
    } 

    /**
     * 修改密码
     */
    public function renew(array $data)
    {
        //验证旧密码是否正确
        if($this->pwd !== hema_hash($data['password'])){
            $this->error = '旧密码输入错误';
            return false;
        }
        //验证密码长度是否合法
		if(strlen($data['password_new'])<6){
			$this->error = '新密码长度不足6位';
            return false;
		}
		if ($data['password_new'] !== $data['password_confirm']) {
            $this->error = '两次输入的新密码不一致';
            return false;
        }
        // 更新管理员信息
        return $this->save([
            'pwd' => hema_hash($data['password_new'])
        ]) !== false;
    }
}
