<?php
namespace app\admin\controller\applet;

use app\admin\controller\Controller;
use app\admin\model\Config as ConfigModel;
use think\facade\View;
use hema\wechat\Driver;

/**
 * 线上模板库管理
 */
class Wxtpl extends Controller
{
    /**
     * 草稿模板列表
     */
    public function index()
    {   
        $wx = new Driver;
        $list = $wx->getTemplateDraftList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 添加到模板库
     */
    public function add($id)
    {
        //把草稿添加到模板库
        $wx = new Driver;
        $result = $wx->addToTemplate($id);
        if($result['errcode'] != 0){
            return $this->renderError('code：' . $result['errcode'] . '，msg：' . $result['errmsg']);
        }
        return $this->renderSuccess('添加成功',url('applet.wxtpl/lists'));
    }
	
    /**
     * 模板库列表
     */
    public function lists()
    {
        $wx = new Driver;
        $list = $wx->getTemplateList();
        return View::fetch('lists', compact('list'));
    }

    /**
     * 删除模板
     */
    public function delete($id)
    {
		$wx = new Driver;
        $result = $wx->deleteTemplateAll($id);
		if($result['errcode']!=0){
			return $this->renderError('code：' . $result['errcode'] . '，msg：' . $result['errmsg']);
		}
		return $this->renderSuccess('删除成功');
    }

}
