<?php
namespace app\store\controller\food\shop;

use app\store\controller\food\Controller;
use app\store\model\Shop as ShopModel;
use app\store\model\ShopClerk as ShopClerkModel;
use app\store\model\Setting;
use think\facade\View;

/**
 * 店员管理控制器
 */
class Clerk extends Controller
{
    /**
     * 列表
     */
    public function index($shop_id = 0, string $search='')
    {
        if(!$this->is_admin OR $this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
		$model = new ShopModel;
        $category = $model->getList(false);
        $model = new ShopClerkModel;
        $list = $model->getList($shop_id,$search);
        return View::fetch('index', compact('list','category','shop_id','search'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            $model = new ShopModel;
            $category = $model->getList(false);
            return View::fetch('add', compact('category'));
        }
        $model = new ShopClerkModel;
        $data = $this->postData('data');
        if(!$this->is_admin OR $this->shop_mode == 10){
            $data['shop_id'] = $this->shop_id;
        }
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('food.shop.clerk/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = ShopClerkModel::get($id);
        if (!$model->remove($id)) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        // 详情
        $model = ShopClerkModel::get($id);
        if (!$this->request->isAjax()) {
            $shop = new ShopModel;
            $category = $shop->getList(false);
            return View::fetch('edit', compact('model', 'category'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('food.shop.clerk/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 店长登录
     */
    public function login()
    {
         if (!$this->request->isAjax()) {
            // 验证登录状态
            if (isset($this->user) AND (int)$this->user['is_login'] === 1 AND (int)$this->user['is_admin'] === 0) {
                return redirect('/store.food.index/index');
            }
            View::layout(false);
            View::assign('web', Setting::getItem('web',0));
            return View::fetch('login');
        }
        $model = new ShopClerkModel;
        if ($model->login($this->postData('data'))) {
            return $this->renderSuccess('登录成功', url('food.index/index'));
        }
        $error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }

    /**
     * 修改管理员密码
     */
    public function renew()
    {
        $model = ShopClerkModel::getClerk([
            'mobile' => $this->user['user']['user_name']
        ]);
        if (!$this->request->isAjax()) {
            return View::fetch();
        }
        if ($model->renew($this->postData('data'))) {
            return $this->renderSuccess('修改成功');
        }
        return $this->renderError($model->getError() ?: '修改失败');
        
    }

}
