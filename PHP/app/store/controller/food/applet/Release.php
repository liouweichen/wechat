<?php
namespace app\store\controller\food\applet;

use app\store\controller\food\Controller;
use app\store\model\AppletTpl as AppletTplModel;
use app\store\model\TemplateCode as TemplateCodeModel;
use app\store\model\Applet as AppletModel;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 小程序模板代码发布管理
 */
class Release extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
    	//获取当前小程序信息
		$applet = AppletModel::detail();
		//获取该小程序发布记录
		$model = new AppletTplModel;
        $list = $model->getList();

		$text = ['还未授权绑定小程序','平台未推送模板','审核中，请等待', '审核延后，请等待', '审核成功，等待发布','已是最新版本','审核被拒绝，请重新发布', '已撤回，请重新发布', '有新版本需要发布'];
		$value = 0;	//还未授权绑定小程序
		$new_tpl = [];

		if($applet['status']['value'] == 1){
			//获取当前平台发布的最新小程序模板代码
			if($new_tpl = TemplateCodeModel::getNew($this->app_type))
			{
				if(sizeof($list) == 0){
					$value = 8;	//有新版本需要发布
				}else{
					if($list[0]['template_code_id'] < $new_tpl['template_code_id']){
						$value = 8;	//有新版本需要发布
					}elseif($list[0]['template_code_id'] == $new_tpl['template_code_id'])
					{
						switch ($list[0]['status']['value']) {
							case 0:
								$value = 2;	//审核中，请等待
								break;
							case 1:
								$value = 6;	//审核被拒绝，请重新发布
								break;
							case 2:
								$value = 5;	//审核成功，已上线（已是最新版本）
								break;
							case 3:
								$value = 4;	//审核成功，等待发布
								break;
							case 4:
								$value = 3;	//审核延后，请等待
								break;
							case 5:
								$value = 7;	//已撤回，请重新发布
								break;
						}
					}
				}
			}else{
				$value = 1;	//平台未推送模板
			}
		}
		$status = ['value' => $value, 'text' => $text[$value]];
        return View::fetch('index', compact('list','new_tpl','status'));
    }
	
	/**
     * 编辑
     */
    public function edit($id)
    {
        $model = AppletTplModel::get($id); //获取模板详情
        //$status = ['审核中', '被拒绝', '已上线', '待发布', '审核延期', '被撤回'];
        if (!$this->request->isAjax()) {
            return View::fetch('edit', compact('model'));
        }
        $data = $this->postData('data');
		if($data['action']==1){
			$wx = new Driver;
			$result = $wx->release($this->applet_id); //上线审核成功的代码
			if($result['errcode'] != 0){
				return $this->renderError('code：'.$result['errcode'].',msg：'.$result['errmsg']);
			}
			$model->edit(['status' => 2]);
			return $this->renderSuccess('上线成功', url('food.applet.release/index'));
		}
		if($data['action']==2){
			//撤回审核
			if($result = $this->undocodeaudit()){
				return $this->renderError('code：'.$result['errcode'].',msg：'.$result['errmsg']);
			}
			$model->edit(['status' => 5]);
			return $this->renderSuccess('撤回成功', url('food.applet.release/index'));
		}
        return $this->renderSuccess('正在返回', url('food.applet.release/index'));
    }

    /**
     * 添加与发布
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
        	//获取最新版本
			$code = TemplateCodeModel::getNew($this->app_type);
            return View::fetch('add', compact('code'));
        }
		if($this->user['applet']['status']['value']==0){
            return $this->renderError('请先绑定您的微信小程序', url('food.applet/index'));
		}
		$model = AppletModel::detail();//获取商户配置信息
		if($model->publish()){
			return $this->renderSuccess('上传成功，等待审核', url('food.applet.release/index'));
		}
		$error = $model->getError() ?: '发布失败';
        return $this->renderError($error);
    }
	
	/**
	* 撤回审核中的小程序
	* 单个帐号每天审核撤回次数最多不超过 1 次，一个月不超过 10 次。
	*/
	private function undocodeaudit()
	{
		$wx = new Driver;
		$result = $wx->undoCodeAudit($this->applet_id);
		if($result['errcode'] == 0){
			return false;
		}
		return $result;
	}
}
