<?php
namespace app\task\controller;

use app\task\model\User as UserModel;
use app\task\model\UserBak as UserBakModel;
use app\task\model\WebSet as WebSetModel;
use app\task\model\Setting as SettingModel;


/**
 * 升级工具
 */

class Tool extends \app\BaseController
{
	/**
     * v4.1.0 升级到 V4.1.1
     */
    public function ()
    {	
    	UserModel::withoutGlobalScope()->where('open_id','')->delete();
    	//设置关注状态
    	UserModel::withoutGlobalScope()->where('open_id','<>','')->update(['is_subscribe' => 1]); 
    	////清理不符合规则的用户
    	UserBakModel::where('avatar','')->delete();
    	$model = new UserBakModel;
    	$user_bak = $model->where([
    		'type' => 10,
    	])->select();
    	$new_user = [];
    	foreach ($user_bak as $item) {
    		$new_user[] = [
    			'union_id' => $item['union_id'],
    			'open_id' => $item['open_id'],
    			'nickname' => $item['nickname'],
    			'avatar' => $item['avatar'],
    			'gender' => $item['gender'],
    			'country' => $item['country'],
    			'province' => $item['province'],
    			'city' => $item['city'],
    			'money' => $item['money'],
    			'pay' => $item['pay'],
    			'score' => $item['score'],
    			'address_id' => $item['address_id'],
    			'recommender' => $item['recommender'],
    			'applet_id' => $item['applet_id'],
    			'status' => 10
    		];
    	}
    	$model = new UserModel;
    	$model->saveAll($new_user);
    	
    	$model = new WebSetModel;
    	$list = $model->select()->toArray();
    	$new_setting = [];
    	foreach ($list as $item) {
    		if($item['key'] == 'delivery'){
        		$key = 'webdelivery';
        	}elseif($item['key'] == 'wxapp'){
        		$key = 'foodhelp';
        	}elseif($item['key'] == 'wxpay'){
        		$key = 'wxpayisp';
        	}elseif($item['key'] == 'payment'){
        		$key = 'webpay';
        	}elseif($item['key'] == 'delivery'){
        		$key = 'webdelivery';
        	}else{
        		$key = $item['key'];
        	}
        	$new_setting[] = [
        		'key' => $key,
        		'describe' => $item['describe'],
        		'values' => $item['values'],
        	];
    	}
    	$model = new SettingModel;
    	$model->saveAll($new_setting);
        echo "升级成功";
	}
}
