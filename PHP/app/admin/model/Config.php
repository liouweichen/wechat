<?php
namespace app\admin\model;

use app\common\model\Config as ConfigModel;
use think\facade\Session;
use hema\wechat\Driver;

/**
 * 站点配置模型
 */
class Config extends ConfigModel
{
    /**
     * 管理员用户登录
     */
    public function login(array $data)
    {
        //体验用户
        if($data['user_name'] =='test' AND $data['password']=='test'){
            // 保存登录状态
            Session::set('hema_admin', [
                'user' => [
                    'user_name' => 'test',
                    'avatar' => base_url() . 'assets/img/avatar.png'
                ],
                'is_login' => true,
            ]);
            return true;
        }
        $filter = [
            'user_name' => $data['user_name'],
            'password' => hema_hash($data['password'])
        ];
        // 验证用户名密码是否正确
        if (!$user = $this->where($filter)->find()){
            $this->error = '登录失败, 用户名或密码错误';
            return false;
        }
        // 保存登录状态
        Session::set('hema_admin', [
            'user' => [
                'user_name' => $user['user_name'],
                'avatar' => base_url() . 'assets/img/avatar.png'
            ],
            'is_login' => true,
        ]);
        return true;
    }
    
    /**
     * 更新当前管理员信息
     */
    public function renew(array $data)
    {       
        if ($data['password'] != $data['password_confirm']) {
            $this->error = '确认密码不正确';
            return false;
        }
        $data['password'] = hema_hash($data['password']);
        // 更新管理员信息
        return $this->save($data) !== false;
    }
	
	/**
     * 编辑
     */
    public function edit(array $data)
    {
        $wx = new Driver;
    	$wx->startTicket($data['app_id'],$data['app_secret']);
        return $this->save($data) !== false;
    }
}
