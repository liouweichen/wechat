<?php
namespace app\store\controller\food\applet;

use app\store\controller\food\Controller;
use app\store\model\AppletName as AppletNameModel;
use app\store\model\Applet as AppletModel;
use think\facade\View;
use hema\wechat\Driver;

/**
 * 设置小程序呢称
 */
class Name extends Controller
{
    /**
     * 设置
     */
    public function setting()
    {
    	$name = AppletNameModel::where('applet_id',$this->applet_id)->order('applet_name_id','desc')->find();
        $model = AppletModel::get($this->applet_id);
        if (!$this->request->isAjax()) {
            return View::fetch('setting', compact('model','name'));
        }
		$model = new AppletNameModel;
		if ($model->action($this->postData('data'),$this->applet_id)) {
			return $this->renderSuccess('更新成功', url('food.applet/index'));
		}
		$error = $model->getError() ?: '更新失败';
		return $this->renderError($error);
    }

    /**
     * 检测名称
     */
    public function checkName($nick_name)
    {
        $wx = new Driver;
        $result = $wx->checkWxVerifyNickName($this->applet_id,$nick_name);
        if($result['errcode']==0){
             if($result['hit_condition']){
                return $this->renderError('命中关键字：'.$result['wording']);
             }
            return $this->renderError('可以使用');
        }
        return $this->renderError('code：'.$result['errcode'].',msg:'.$result['errmsg']);
    }
}
